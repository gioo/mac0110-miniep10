
function max_sum_submatrix(matrix)
    total=length(matrix)
    squares=[]
    side=1
    while side<=total
        if side^2<=total
            push!(squares,side)
        end
        side+=1
    end
    sum=0
    sub_matrix=[]
    new_sum=0
    new_sub_matrix=sub_matrix
    for square in squares
        for row = 1:(size(matrix,1)-square+1)
            for column = 1:(size(matrix,2)-square+1)
                new_sum=0
                new_sub_matrix=zeros(square,square)
                i=row
                i_new=1
                while i_new<=square
                    j=column
                    j_new=1
                    while j_new<=square
                        new_sub_matrix[i_new,j_new]=matrix[i,j]
                        new_sum+=matrix[i,j]
                        j+=1
                        j_new+=1
                    end
                    i+=1
                    i_new+=1
               end
                if new_sum>=sum
                    sum=new_sum
                    push!(sub_matrix,new_sub_matrix)
                end
            end
        end
    end
    sum=0
    for possible_square in sub_matrix
        greatest_sum=0
        for element in possible_square
            greatest_sum+=element
        end
        if greatest_sum>=sum
            sum=greatest_sum
        end
    end
    greatest_squares=[]
    for possible_square in sub_matrix
        total_sum=0
        for element in possible_square
            total_sum+=element
        end
        if total_sum==sum
            push!(greatest_squares,possible_square)
        end
    end
    return greatest_squares
end
